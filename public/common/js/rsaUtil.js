//RSA加解密

//加密
function doRSAEncrypt(publicKey, text){
	
	//設定公鑰
	var encrypt = new JSEncrypt();
	encrypt.setPublicKey(publicKey);
	
	//加密
	var result = encrypt.encrypt(text);

	//結果
	console.log('doRSAEncrypt: ' + result);
    return result;
}

//解密
function doRSADecrypt(privateKey, text){

	//設定私鑰
	var encrypt = new JSEncrypt();
	encrypt.setPrivateKey(privateKey);
	
	//解密
	var result = encrypt.decrypt(text);

	//結果
	console.log('doRSADecrypt: ' + result);
    return result;
}