//AES加解密
function doAESEncrypt(key, text){

	//轉換格式
	var keyFormat  = CryptoJS.enc.Base64.parse(key);
	var textFormat = CryptoJS.enc.Utf8.parse(text);

    //加密
	var encrypted = CryptoJS.AES.encrypt(textFormat, keyFormat, {
        mode: CryptoJS.mode.ECB, 
        padding: CryptoJS.pad.Pkcs7
    });
	
	//結果
	var result = encrypted.ciphertext.toString().toLowerCase();
	console.log('encrypted1: ' + result);
	
    return result;
}

function doAESDecrypt(key, text){

	//轉換格式
	var keyFormat  = CryptoJS.enc.Base64.parse(key);
	var textFormat = CryptoJS.enc.Hex.parse(text);
	textFormat = CryptoJS.enc.Base64.stringify(textFormat);
	
	//解密
	var decrypted = CryptoJS.AES.decrypt(textFormat, keyFormat, {
        mode: CryptoJS.mode.ECB, 
        padding: CryptoJS.pad.Pkcs7
    });
	
	//結果
	var result = decrypted.toString(CryptoJS.enc.Utf8);
	console.log("doAESDecrypt result: " + result.toString());
	return result;
}