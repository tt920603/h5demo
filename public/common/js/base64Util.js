//BASE64加密
function doBase64Encrypt(text){
	var wordArray = CryptoJS.enc.Utf8.parse(text);
	var base64 = CryptoJS.enc.Base64.stringify(wordArray);
	return base64;
}
//BASE64解密
function doBase64Decrypt(text){
	var parsedWordArray = CryptoJS.enc.Base64.parse(text);
	var parsedStr = parsedWordArray.toString(CryptoJS.enc.Utf8);
	return parsedStr;
}